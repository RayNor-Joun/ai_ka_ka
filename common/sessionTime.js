function compareTime(startTime, endTime) {

	var s = startTime.substr(0, 10);
	var e = endTime.substr(0, 10);
	if (e > s) { //不是同一天的时间  seesion 已失效
		return false;
	} else {
		var startTimes = startTime.substring(0, 10).split('-');
		var endTimes = endTime.substring(0, 10).split('-');
		startTime = startTimes[1] + '-' + startTimes[2] + '-' + startTimes[0] + ' ' + startTime.substring(10, 19);
		endTime = endTimes[1] + '-' + endTimes[2] + '-' + endTimes[0] + ' ' + endTime.substring(10, 19);
		var leave1 = (Date.parse(endTime) - Date.parse(startTime));
		var hours = Math.floor(leave1 / (3600 * 1000))
		var leave2 = leave1 % (3600 * 1000) //计算小时数后剩余的毫秒数
		var minutes = Math.floor(leave2 / (60 * 1000))

		if (minutes >= 15) { //15分钟的有效期
			return false;
		} else {
			return true;
		}
	}
}

//格式化后日期为：yyyy-MM-dd HH:mm:ss
function FormatAllDate(sDate) {
	var date = new Date(sDate);
	var seperator1 = "-";
	var seperator2 = ":";
	var month = date.getMonth() + 1;
	var strDate = date.getDate();
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();
	//月
	if (month >= 1 && month <= 9) {
		month = "0" + month;
	}
	//日
	if (strDate >= 0 && strDate <= 9) {
		strDate = "0" + strDate;
	}
	//时
	if (hours >= 0 && hours <= 9) {
		hours = "0" + hours;
	}
	//分
	if (minutes >= 0 && minutes <= 9) {
		minutes = "0" + minutes;
	}
	//秒
	if (seconds >= 0 && seconds <= 9) {
		seconds = "0" + seconds;
	}
	//格式化后日期为：yyyy-MM-dd HH:mm:ss
	var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate +
		" " + hours + seperator2 + minutes + seperator2 + seconds;
	return currentdate;
}
module.exports = {
	compareTime: compareTime,
	FormatAllDate: FormatAllDate
}
