function wx() {
	//微信官方js判断做法 判断当前页面是否在微信浏览器中打开
	var ua = navigator.userAgent.toLowerCase();
	var isWeixin = ua.indexOf('micromessenger') != -1;
	if (isWeixin == false) {
		uni.showToast({
			title: "请在微信浏览器中打开",
			duration:3000,
			icon: "none"
		})
		return isWeixin;
	}
}
module.exports = {
	wx: wx
}
